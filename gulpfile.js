var elixir = require('laravel-elixir');

/*
 |--------------------------------------------------------------------------
 | Elixir Asset Management
 |--------------------------------------------------------------------------
 |
 | Elixir provides a clean, fluent API for defining some basic Gulp tasks
 | for your Laravel application. By default, we are compiling the Sass
 | file for our application, as well as publishing vendor resources.
 |
 */

elixir(function(mix) {

	/*
	 * Mix the Sass including Zurb Foundation
	 */
    mix.sass(
    	'app.scss',
    	'public/css',
    	{
    		includePaths: ['bower_components/foundation-sites/scss']
    	}
    );

    /*
     * Mix the scripts including Zurb Foundations
     */
    mix.scripts(
    	[
            'jquery/dist/jquery.min.js', 
            'foundation-sites/dist/foundation.min.js'
        ],
    	'public/js/app.js',
        'bower_components/'
    );


});
