<?php 
$I = new FunctionalTester($scenario);
$I->am('an Admin');
$I->wantTo('view a course as an admin and a course leader');

Auth::loginUsingId(11);
$I->seeAuthentication();

// What page I am on
$I->amOnPage('/courses');
$I->see('Computing', 'a');
$I->click('Computing', 'a');

$I->seeRecord('courses', [
	'title' => 'Computing'
]);

// When on the computing page
$I->amOnPage('/courses/1');
$I->see('Module 1');
$I->see('Module 2');
$I->see('Module 3');
$I->see('Module 4');
$I->see('Module 5');
$I->see('Module 6');
$I->see('Module 7');
$I->see('Module 8');
$I->see('Module 9');
$I->see('Module 10');

// View the buttons
$I->see('Delete Course', '.button');
$I->see('Edit Course', '.button');
$I->see('Add Module', '.button');

// Logout and Login as course leader
Auth::logout();
Auth::loginUsingId(13);
$I->seeAuthentication();

$I->amOnPage('/courses');
$I->see('Computing');
$I->dontSee('Web Development');

$I->click('Computing');
$I->see('Module 1');
$I->see('Module 2');
$I->see('Module 3');
$I->see('Module 4');
$I->see('Module 5');
$I->see('Module 6');
$I->see('Module 7');
$I->see('Module 8');
$I->see('Module 9');
$I->see('Module 10');

// Check for course Stats
$I->expect('to see a progress bar');
$I->seeElement('.progress');
$I->see('Progress:');
$I->see('Number of Modules:');
$I->see('Completed Modules:');

Auth::logout();