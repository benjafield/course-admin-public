<?php 
$I = new FunctionalTester($scenario);
$I->am('a Course Leader');
$I->wantTo('view course progress');

// Login
Auth::loginUsingId(13);
$I->seeAuthentication();

// Actions
$I->amOnPage('/courses');
$I->see('Computing');
$I->see('Leader: Course Leader');
$I->seeElement('.progress');
$I->see('90%', '.progress-meter-text');
$I->click('Computing');

// Should switch page
$I->seeCurrentUrlEquals('/courses/1');
$I->see('Module 1');
$I->see('Module 2');
$I->see('Module 3');
$I->see('Module 4');
$I->see('Module 5');
$I->see('Module 6');
$I->see('Module 7');
$I->see('Module 8');
$I->see('Module 9');
$I->see('Module 10');
$I->see('Progress:');
$I->see('90%', '.progress-meter-text');