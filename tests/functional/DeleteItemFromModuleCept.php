<?php 
$I = new FunctionalTester($scenario);
$I->am('a Module Leader');
$I->wantTo('Delete an item from a module');

// Login
Auth::loginUsingId(12);
$I->seeAuthentication();

// Actions
$I->amOnPage('/modules/1');
$I->see('Item 1');
$I->see('Delete', '#deleteItem1');
$I->click('#deleteItem1');
$I->seeCurrentUrlEquals('/itemmodule/1');
$I->see('Yes, Delete');
$I->seeRecord('item_module', [
	'item_id'   => 1,
	'module_id' => 1
]);
$I->click('Yes, Delete');
$I->dontSeeRecord('item_module', [
	'item_id'   => 1,
	'module_id' => 1
]);
$I->seeCurrentUrlEquals('/modules/1');
$I->dontSee('Item 1');