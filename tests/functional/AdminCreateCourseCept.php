<?php 
$I = new FunctionalTester($scenario);
$I->am('an Admin');
$I->wantTo('create a course');

// Login as the Admin
Auth::loginUsingId(11);
$I->seeAuthentication();

// What page am I on?
$I->amOnPage('/');

// I can see
$I->see('Welcome, Admin!', 'h1');
$I->see('Create Course', 'a');

// Actions
$I->click('Create Course', 'a');

// What happens?
$I->amOnPage('/courses/create');
$I->see('Create a course', 'h1');

// Fill out the form
$I->submitForm('#createCourseForm', [
	'title'     => 'CodeCeption Admin Test',
	'code'      => 'BIS99999',
	'leader_id' => 13
]);

// Check that the new record is in the DB
$I->seeRecord('courses', [
	'title' => 'CodeCeption Admin Test'
]);

// Result
$I->amOnPage('/courses');
$I->see('CodeCeption Admin Test', 'a');