<?php 
$I = new FunctionalTester($scenario);
$I->am('a Module Leader');
$I->wantTo('add an item to a module');

// Login
Auth::loginUsingId(12);
$I->seeAuthentication();

// Actions
$I->amOnPage('/modules/1');
$I->see('Module 1');
$I->see('Add Item', '.button');
$I->click('Add Item');

$I->seeCurrentUrlEquals('/modules/1/item');
$I->seeElement('#newModuleItemForm');
$I->submitForm('#newModuleItemForm', [
	'text' => 'CodeCeption Module Item'
]);
$I->amOnPage('/modules/1');
$I->seeRecord('items', [
	'text' => 'CodeCeption Module Item'
]);
$I->see('CodeCeption Module Item');