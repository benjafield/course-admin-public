<?php 
$I = new FunctionalTester($scenario);
$I->am('a module leader');
$I->wantTo('perform actions and see result');

// Login
Auth::loginUsingId(12);
$I->seeAuthentication();

// Actions
$I->amOnPage('/modules/1');
$I->see('Complete', '.button');
$I->see('Incomplete', '.button');

// Mark Completed as Incomplete
$I->click('Incomplete');
$I->seeNumberOfElements('button.success', 2);

$I->click('Complete');
$I->see('Incomplete');