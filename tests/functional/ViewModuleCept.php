<?php 
$I = new FunctionalTester($scenario);
$I->am('an Admin');
$I->wantTo('view a module as admin, module and course leaders');

// Login
Auth::loginUsingId(11);
$I->seeAuthentication();

// Actions
$I->amOnPage('/modules');
$I->see('Module 1');
$I->click('Module 1');

$I->amOnPage('/modules/1');
$I->seeRecord('modules', [
	'title' => 'Module 1'
]);
$I->see('Module 1');
$I->see('Items', 'h2');
$I->see('Statistics', 'h2');
$I->see('Add Item', '.button');
$I->see('Edit Module', '.button');
$I->see('Delete Module', '.button');
$I->see('50%');

// Logout and login as module leader
Auth::logout();
Auth::loginUsingId(12);
$I->seeAuthentication();

$I->amOnPage('/modules/1');
$I->see('Add Item', '.button');

// Logout and login as course leader
Auth::logout();
Auth::loginUsingId(13);
$I->seeAuthentication();

$I->amOnPage('/modules/1');