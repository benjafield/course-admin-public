<?php 
$I = new FunctionalTester($scenario);
$I->am('a Course Leader');
$I->wantTo('delete an Item and view confirm delete');

// Login
Auth::loginUsingId(13);
$I->seeAuthentication();

// Actions
$I->amOnPage('/items');
$I->see('Item 1');
$I->see('Item 2');
$I->see('New Item');
$I->see('Delete', '#deleteItem1');
$I->seeRecord('items', [
	'id' => 1
]);
$I->click('#deleteItem1');
$I->see('Yes, Delete');
$I->click('Yes, Delete');
$I->dontSeeRecord('items', [
	'id' => 1
]);
