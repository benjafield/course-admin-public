-- phpMyAdmin SQL Dump
-- version 4.5.0.2
-- http://www.phpmyadmin.net
--
-- Host: localhost
-- Generation Time: Jan 04, 2016 at 11:53 PM
-- Server version: 5.6.27-0ubuntu0.14.04.1
-- PHP Version: 5.6.14-1+deb.sury.org~trusty+1

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";

--
-- Database: `courseadmin`
--

-- --------------------------------------------------------

--
-- Table structure for table `courses`
--

CREATE TABLE IF NOT EXISTS `courses` (
  `id` int(10) UNSIGNED NOT NULL,
  `title` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `code` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `leader_id` int(10) UNSIGNED NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00'
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Truncate table before insert `courses`
--

TRUNCATE TABLE `courses`;
--
-- Dumping data for table `courses`
--

INSERT INTO `courses` (`id`, `title`, `code`, `leader_id`, `created_at`, `updated_at`) VALUES
(1, 'Computing', 'BIS2673', 13, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(2, 'Software & Systems', 'BIS6978', 8, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(3, 'Web Development', 'BIS4153', 10, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(4, 'App Development', 'BIS2222', 1, '0000-00-00 00:00:00', '0000-00-00 00:00:00');

-- --------------------------------------------------------

--
-- Table structure for table `course_module`
--

CREATE TABLE IF NOT EXISTS `course_module` (
  `course_id` int(10) UNSIGNED NOT NULL,
  `module_id` int(10) UNSIGNED NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00'
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Truncate table before insert `course_module`
--

TRUNCATE TABLE `course_module`;
--
-- Dumping data for table `course_module`
--

INSERT INTO `course_module` (`course_id`, `module_id`, `created_at`, `updated_at`) VALUES
(1, 1, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(3, 1, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(4, 1, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(1, 2, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(3, 2, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(4, 2, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(1, 3, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(4, 3, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(1, 4, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(4, 4, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(1, 5, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(1, 6, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(2, 6, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(4, 6, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(1, 7, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(3, 7, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(4, 7, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(1, 8, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(3, 8, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(4, 8, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(1, 9, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(3, 9, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(1, 10, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(3, 10, '0000-00-00 00:00:00', '0000-00-00 00:00:00');

-- --------------------------------------------------------

--
-- Table structure for table `items`
--

CREATE TABLE IF NOT EXISTS `items` (
  `id` int(10) UNSIGNED NOT NULL,
  `text` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `default` tinyint(1) NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00'
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Truncate table before insert `items`
--

TRUNCATE TABLE `items`;
-- --------------------------------------------------------

INSERT INTO `items` (`id`, `text`, `default`, `created_at`, `updated_at`) VALUES (NULL, 'Item 1', '0', '2015-12-16 00:00:00.000000', '0000-00-00 00:00:00.000000'), (NULL, 'Item 2', '0', '2015-12-16 00:00:00', '0000-00-00 00:00:00.000000');

--
-- Table structure for table `item_module`
--

CREATE TABLE IF NOT EXISTS `item_module` (
  `item_id` int(10) UNSIGNED NOT NULL,
  `module_id` int(10) UNSIGNED NOT NULL,
  `completed` tinyint(1) NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00'
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Truncate table before insert `item_module`
--

TRUNCATE TABLE `item_module`;
-- --------------------------------------------------------

INSERT INTO `item_module` (`item_id`, `module_id`, `completed`, `created_at`, `updated_at`) VALUES ('1', '1', '1', '0000-00-00 00:00:00.000000', '0000-00-00 00:00:00.000000'), ('2', '1', '0', '0000-00-00 00:00:00.000000', '0000-00-00 00:00:00.000000');

--
-- Table structure for table `migrations`
--

CREATE TABLE IF NOT EXISTS `migrations` (
  `migration` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `batch` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Truncate table before insert `migrations`
--

TRUNCATE TABLE `migrations`;
-- --------------------------------------------------------

--
-- Table structure for table `modules`
--

CREATE TABLE IF NOT EXISTS `modules` (
  `id` int(10) UNSIGNED NOT NULL,
  `title` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `code` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `leader_id` int(10) UNSIGNED NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00'
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Truncate table before insert `modules`
--

TRUNCATE TABLE `modules`;
--
-- Dumping data for table `modules`
--

INSERT INTO `modules` (`id`, `title`, `code`, `leader_id`, `created_at`, `updated_at`) VALUES
(1, 'Module 1', 'CIS9828', 12, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(2, 'Module 2', 'CIS8482', 7, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(3, 'Module 3', 'CIS8587', 4, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(4, 'Module 4', 'CIS4247', 1, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(5, 'Module 5', 'CIS4372', 3, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(6, 'Module 6', 'CIS5692', 4, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(7, 'Module 7', 'CIS7904', 3, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(8, 'Module 8', 'CIS7732', 2, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(9, 'Module 9', 'CIS8833', 8, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(10, 'Module 10', 'CIS6848', 1, '0000-00-00 00:00:00', '0000-00-00 00:00:00');

-- --------------------------------------------------------

--
-- Table structure for table `password_resets`
--

CREATE TABLE IF NOT EXISTS `password_resets` (
  `email` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `token` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00'
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Truncate table before insert `password_resets`
--

TRUNCATE TABLE `password_resets`;
-- --------------------------------------------------------

--
-- Table structure for table `permissions`
--

CREATE TABLE IF NOT EXISTS `permissions` (
  `id` int(10) UNSIGNED NOT NULL,
  `name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `label` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `created_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00'
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Truncate table before insert `permissions`
--

TRUNCATE TABLE `permissions`;
--
-- Dumping data for table `permissions`
--

INSERT INTO `permissions` (`id`, `name`, `label`, `created_at`, `updated_at`) VALUES
(1, 'manage_courses', 'Manage Courses', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(2, 'manage_items', 'Manage Items', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(3, 'manage_modules', 'Manage Modules', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(4, 'manage_users', 'Manage Users', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(5, 'view_modules', 'View Modules', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(6, 'view_module', 'View Module', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(7, 'complete_items', 'Mark Items Complete', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(8, 'delete_item', 'Delete Item', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(9, 'item_module', 'Add Item To Module', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(10, 'view_courses', 'View Courses', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(11, 'view_course', 'View Course', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(12, 'delete_course', 'Delete Course', '0000-00-00 00:00:00', '0000-00-00 00:00:00');

-- --------------------------------------------------------

--
-- Table structure for table `permission_role`
--

CREATE TABLE IF NOT EXISTS `permission_role` (
  `permission_id` int(10) UNSIGNED NOT NULL,
  `role_id` int(10) UNSIGNED NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Truncate table before insert `permission_role`
--

TRUNCATE TABLE `permission_role`;
--
-- Dumping data for table `permission_role`
--

INSERT INTO `permission_role` (`permission_id`, `role_id`) VALUES
(1, 1),
(2, 1),
(3, 1),
(4, 1),
(8, 1),
(12, 1),
(5, 2),
(6, 2),
(7, 2),
(8, 2),
(9, 2),
(2, 3),
(5, 3),
(6, 3),
(10, 3),
(11, 3);

-- --------------------------------------------------------

--
-- Table structure for table `roles`
--

CREATE TABLE IF NOT EXISTS `roles` (
  `id` int(10) UNSIGNED NOT NULL,
  `name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `label` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `created_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00'
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Truncate table before insert `roles`
--

TRUNCATE TABLE `roles`;
--
-- Dumping data for table `roles`
--

INSERT INTO `roles` (`id`, `name`, `label`, `created_at`, `updated_at`) VALUES
(1, 'admin', 'Admin', '2016-01-04 23:46:11', '2016-01-04 23:46:11'),
(2, 'module_leader', 'Module Leader', '2016-01-04 23:46:11', '2016-01-04 23:46:11'),
(3, 'course_leader', 'Course Leader', '2016-01-04 23:46:11', '2016-01-04 23:46:11');

-- --------------------------------------------------------

--
-- Table structure for table `role_user`
--

CREATE TABLE IF NOT EXISTS `role_user` (
  `role_id` int(10) UNSIGNED NOT NULL,
  `user_id` int(10) UNSIGNED NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Truncate table before insert `role_user`
--

TRUNCATE TABLE `role_user`;
--
-- Dumping data for table `role_user`
--

INSERT INTO `role_user` (`role_id`, `user_id`) VALUES
(1, 11),
(2, 12),
(3, 13);

-- --------------------------------------------------------

--
-- Table structure for table `users`
--

CREATE TABLE IF NOT EXISTS `users` (
  `id` int(10) UNSIGNED NOT NULL,
  `name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `email` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `password` varchar(60) COLLATE utf8_unicode_ci NOT NULL,
  `remember_token` varchar(100) COLLATE utf8_unicode_ci DEFAULT NULL,
  `created_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00'
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Truncate table before insert `users`
--

TRUNCATE TABLE `users`;
--
-- Dumping data for table `users`
--

INSERT INTO `users` (`id`, `name`, `email`, `password`, `remember_token`, `created_at`, `updated_at`) VALUES
(1, 'Jameson Klocko II', 'Rutherford.Christelle@gmail.com', '$2y$10$nzYf0Rlero5EcKkPpyqDPeWSNristR3tjPvrY4ymDhWLEyv7eXjSa', '1Jx43F8cho', '2016-01-04 23:46:10', '2016-01-04 23:46:10'),
(2, 'Ronaldo Bauch', 'Leda.Effertz@hotmail.com', '$2y$10$C8jqi/zf.JR28T4FPPk0eeQbxDeovwJVHwG5P3r7.fkp1HQCefEnq', 'eJVJw2nLTw', '2016-01-04 23:46:10', '2016-01-04 23:46:10'),
(3, 'Prof. Kamille Yost', 'Walsh.Rozella@Parisian.com', '$2y$10$4cCoFnjPCfXTho6gP393WObyGcXUu37QFFvqkeQA0ljbDbWf94oE6', 'SQ9KDZhTBy', '2016-01-04 23:46:10', '2016-01-04 23:46:10'),
(4, 'Matilda Gislason', 'Yadira29@gmail.com', '$2y$10$/WdQMSzKf1E49tD8E88HEe/oN7FfPl.eNw3Fkqz4/W.JMbtuI1kIS', 'mVrDgvGWBJ', '2016-01-04 23:46:10', '2016-01-04 23:46:10'),
(5, 'Dr. Harrison Hyatt MD', 'Napoleon.Kihn@Kutch.com', '$2y$10$o9/sDmVHcu2tfAIjEywF2uLY0u5P2mBf2NHwrgoANefOv0PL3u3ae', 'Tn7HKMu6nU', '2016-01-04 23:46:10', '2016-01-04 23:46:10'),
(6, 'Aaron Pfeffer I', 'Bernadette27@gmail.com', '$2y$10$p48vu9IUqW6Ti3kxanL8h.U7lP735vP93flcPXqI43H3tzNjEp386', 'kQReQjKKah', '2016-01-04 23:46:10', '2016-01-04 23:46:10'),
(7, 'Pablo O''Keefe', 'Katelin60@DuBuque.com', '$2y$10$o/5SOCLX16oBwg0Jf1GiAuxKjXZkwdJKiuuFOQMTAHuKTOBbd0lM2', 'fo0PxV28Br', '2016-01-04 23:46:10', '2016-01-04 23:46:10'),
(8, 'Zion McCullough DVM', 'Darron55@yahoo.com', '$2y$10$kwLW40TFJgJ8zVvINCIHxuXjsAcPE.bPmUKdJiRgiincvNvz.h08.', 'U5B2Auk11p', '2016-01-04 23:46:10', '2016-01-04 23:46:10'),
(9, 'Ivory Wisozk', 'Kuhic.Ryan@Koelpin.net', '$2y$10$vx3klfERKIu7iiAFWsXJIe52VzXx9JCpVd.CQ9IorcviMCIYijiai', 'Vi3dQhsk8X', '2016-01-04 23:46:10', '2016-01-04 23:46:10'),
(10, 'Lora Kirlin', 'Shanny36@Gibson.com', '$2y$10$3E1KR4s2OMHtsV.zDghOrueLRN.IiCLdC.M9cfqQllxF1LK5fsKKW', 'cYyw8pewHg', '2016-01-04 23:46:10', '2016-01-04 23:46:10'),
(11, 'Admin', 'admin@example.com', '$2y$10$IilelwIVVLhE7i2hTZAMpeamSCb4YCh3P6jH/nKAssmMz3TznFDLa', 'vjKKlYJSLzLpVZEXR9ESK5IV4ZSAdPVP5QJHAVOmZ4AcccujcNQzvTVUDBGV', '2016-01-04 23:46:10', '2016-01-04 23:47:00'),
(12, 'Module Leader', 'moduleleader@example.com', '$2y$10$QP3KnAbE3afmEEtKmqG2n..3fkj7M.ZxRqHGwWsQlNlpmi3wsIUUW', NULL, '2016-01-04 23:46:10', '2016-01-04 23:46:10'),
(13, 'Course Leader', 'courseleader@example.com', '$2y$10$N3fa/LavdrgXUskRiPCxyuQiSYklkoH8OfKUhNNQqxpe.m1JErRCq', NULL, '2016-01-04 23:46:11', '2016-01-04 23:46:11');