<?php 
$I = new AcceptanceTester($scenario);

// Who I am
$I->am('an admin');
// What I want to do
$I->wantTo('login as an admin and logout again');

// Elements of the page
$I->amOnPage('/');
$I->see('Login', 'h1');
$I->seeElement('#email');
$I->seeElement('#password');
$I->see('Login', 'button');

// Testing the form
$I->amGoingTo('fill out the login form with admin credentials');
$I->fillField('email', 'admin@example.com'); // Admin Email
$I->fillField('password', 'password'); // Admin Password
$I->click('Login');

// Result
$I->expect('to be logged in and see the welcome greeting');
$I->see('CourseAdmin', '.menu-text');
$I->see('Welcome, Admin!', 'h1');

$I->expect('the admin dashboard buttons');
$I->see('Create Course', '.button');
$I->see('Create Module', '.button');
$I->see('Create Item', '.button');

// Logging Out Again
$I->amGoingTo('logout of the application');
$I->see('Logout', 'a');
$I->click('Logout');

// Logout Result
$I->expect('to view the login heading once again');
$I->see('Login', 'h1');