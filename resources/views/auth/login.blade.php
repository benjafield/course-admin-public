@extends('layouts.main')
@section('title', 'Login')

@section('content')
	
	<div class="row">
		<div class="large-12 columns">
			<h1>Login</h1>
		</div>
	</div>

	{!! Form::open(['url' => 'login', 'autocomplete' => 'off']) !!}
	
	<div class="row">
		<div class="large-12 columns">
			<label>
				Email Address
				<input type="text" name="email" placeholder="Email Address" id="email">
			</label>
		</div>
	</div>

	<div class="row">
		<div class="large-12 columns">
			<label>
				Password
				<input type="password" name="password" placeholder="Password" id="password">
			</label>
		</div>
	</div>

	<div class="row">
		<div class="large-12 columns">
			<label>
				<input type="checkbox" name="remember" value="1"> Remember?
			</label>
		</div>
	</div>

	<div class="row">
		<div class="large-12 columns">
			<button type="submit" class="button expanded">Login</button>
		</div>
	</div>

	{!! Form::close() !!}

@stop