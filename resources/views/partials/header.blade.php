<header class="top-bar" id="appMenu">
    <div class="top-bar-left">
        <ul class="dropdown menu" data-dropdown-menu>
            <li class="menu-text">CourseAdmin</li>
            @if (Auth::check())
            <li><a href="{{ url() }}">Dashboard</a></li>
            @if (Auth::user()->hasRole('admin') || Auth::user()->hasRole('course_leader'))
            <li><a href="{{ url('courses') }}">Courses</a></li>
            @endif
            <li><a href="{{ url('modules') }}">Modules</a></li>
            <li><a href="{{ url('items') }}">Items</a></li>
            @can('manage_users')
            <li><a href="{{ url('users') }}">Users</a></li>
            @endcan
            <li><a href="{{ url('logout') }}">Logout</a></li>
            @endif
        </ul>
    </div>
</header>