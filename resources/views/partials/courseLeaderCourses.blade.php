@foreach(Auth::user()->courses as $course)
<div class="row">
	<div class="large-12 columns callout secondary">
		<h5><a href="{{ url('courses/' . $course->id) }}">{{ $course->title }}</a></h5>
		<p>Leader: {{ $course->leader->name }}</p>

		<?php
			if (isset($completedModules[$course->id]))
			{
				$countCompleted = count($completedModules[$course->id]);
				$totalModules = $course->modules->count();
				$percentage = (($totalModules > 0) ? Maths::rPercentage($countCompleted, $totalModules) : 0);
			}
			else
			{
				$percentage = 0;
			}
		?>

		<div class="progress" role="progressbar">
			<div class="progress-meter" style="width:{{ $percentage }}%">
				 <p class="progress-meter-text">{{ (($percentage > 0) ? $percentage . '%' : '') }}</p>
			</div>
		</div>
	</div>
</div>
@endforeach