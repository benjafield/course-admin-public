<div class="row">
	<div class="large-12 columns">
		<label>
			Title
			{!! Form::text('title', old('title')) !!}
		</label>
	</div>
</div>

<div class="row">
	<div class="large-12 columns">
		<label>
			Code
			{!! Form::text('code', old('code')) !!}
		</label>
	</div>
</div>

<div class="row">
	<div class="large-12 columns">
		<label>
			Module Leader
			<select name="leader_id">
				@foreach ($users as $leader)
				<option value="{{ $leader->id }}" {!! (((isset($module->leader_id)) && $module->leader_id == $leader->id) ? 'selected': '') !!}>{{ $leader->name }}</option>
				@endforeach
			</select>
		</label>
	</div>
</div>

<div class="row">
	<div class="large-12 columns">
		<button type="submit" class="button expanded">Save Module</button>
	</div>
</div>