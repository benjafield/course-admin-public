<div class="row">
	<div class="large-12 columns">
		<div class="callout alert">
			<h3>Are you sure you want to delete {{ $label }}?</h3>
			<div class="button-group">
				{!! Form::hidden('deleteConfirmed', 1) !!}
				<a href="{{ url($abort) }}" class="button warning">No, Go Back</a>
				<button class="button alert" type="submit">Yes, Delete</button>
			</div>
		</div>
	</div>
</div>