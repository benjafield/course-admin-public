@if (count($errors) > 0)
<div class="row">
	<div class="large-12 columns callout alert">
		<h5>Validation Errors:</h5>
		@foreach ($errors->all() as $error)
		<p>{{ $error }}</p>
		@endforeach
	</div>
</div>
@endif

<div class="row">
	<div class="large-12 columns">
		<label>
			Item Text
			{!! Form::text('text', old('text')) !!}
		</label>
	</div>
</div>

<div class="row">
	<div class="large-12 columns">
		{!! Form::hidden('default', 0) !!}
		@if (!isset($default) || $default != FALSE)
			{!! Form::checkbox('default', 1, FALSE, ['id' => 'default']) !!}
			{!! Form::label('default', 'Default') !!}
		@endif
	</div>
</div>

<div class="row">
	<div class="large-12 columns">
		@if (isset($module_id))
		{!! Form::hidden('module_id', $module_id) !!}
		@endif
		<button type="submit" class="button expanded">Save Item</button>
	</div>
</div>