@if (count($errors) > 0)
<div class="row">
	<div class="large-12 columns callout alert">
		<h5>Validation Errors:</h5>
		@foreach ($errors->all() as $error)
		<p>{{ $error }}</p>
		@endforeach
	</div>
</div>
@endif

<div class="row">
	<div class="large-12 columns">
		<label>
			Title
			{!! Form::text('title', old('title')) !!}
		</label>
	</div>
</div>

<div class="row">
	<div class="large-12 columns">
		<label>
			Code
			{!! Form::text('code', old('code')) !!}
		</label>
	</div>
</div>

<div class="row">
	<div class="large-12 columns">
		<label>
			Course Leader
			<select name="leader_id">
				@foreach ($users as $leader)
				<option value="{{ $leader->id }}" {!! (((isset($course->leader_id)) && $course->leader_id == $leader->id) ? 'selected': '') !!}>{{ $leader->name }}</option>
				@endforeach
			</select>
		</label>
	</div>
</div>

<div class="row">
	<div class="large-12 columns">
		<button type="submit" class="button expanded">Save Course</button>
	</div>
</div>