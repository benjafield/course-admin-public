@if (count($errors) > 0)
<div class="row">
	<div class="large-12 columns callout alert">
		<h5>Validation Errors:</h5>
		@foreach ($errors->all() as $error)
		<p>{{ $error }}</p>
		@endforeach
	</div>
</div>
@endif

<div class="row">
	<div class="large-12 columns">
		<label>
			Name
			{!! Form::text('name', old('name')) !!}
		</label>
	</div>
</div>

<div class="row">
	<div class="large-12 columns">
		<label>
			Email
			{!! Form::email('email', old('email')) !!}
		</label>
	</div>
</div>

<div class="row">
	<div class="large-12 columns">
		<label>
			Password
			{!! Form::password('password') !!}
		</label>
	</div>
</div>

<div class="row">
	<div class="large-12 columns">
		<label>
			Role
			<select name="user_role">
				@foreach($roles as $role)
				<option value="{{ $role->id }}" {!! ((isset($user->roles[0]->id) && $user->roles[0]->id == $role->id) ? 'selected' : '') !!}>{{ $role->label }}</option>
				@endforeach
			</select>
		</label>
	</div>
</div>

<div class="row">
	<div class="large-12 columns">
		<button class="button" type="submit">Save User</button>
	</div>
</div>