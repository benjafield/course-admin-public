@extends('layouts.main')
@section('title', Auth::user()->name)

@section('content')

	<div class="row">
		<div class="large-12">
			<h1>Welcome, {{ Auth::user()->name }}!</h1>
			<hr>
			@if (Auth::user()->hasRole('admin'))
			<div class="button-group">
				<a href="{{ url('courses/create') }}" class="button">Create Course</a>
				<a href="{{ url('modules/create') }}" class="button">Create Module</a>
				<a href="{{ url('items/create') }}" class="button">Create Item</a>
			</div>
			@endif
			@if (Auth::user()->hasRole('module_leader'))
			<div class="button-group">
				<a href="{{ url('modules') }}" class="button">My Modules</a>
			</div>
			@endif
		</div>
	</div>

@stop