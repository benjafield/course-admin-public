@extends('layouts.main')
@section('title', 'Delete Item')

@section('content')

<div class="row">
	<div class="large-12 columns">
		<h1>Delete Item: {{ $item->text }}</h1>
		<hr>
	</div>
</div>

{!! Form::open(['route' => ['items.destroy', $item->id], 'method' => 'DELETE']) !!}

@include('partials.confirmDelete', ['label' => 'the item: ' . $item->text, 'abort' => 'items'])

{!! Form::close() !!}

@stop