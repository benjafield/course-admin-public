@extends('layouts.main')
@section('title', 'Create Item')

@section('content')

<div class="row">
	<div class="large-12 columns">
		<h1>Create Item</h1>
		<hr>
	</div>
</div>

{!! Form::open(['url' => 'items']) !!}
@include('partials.itemForm', ['module_id' => ''])
{!! Form::close() !!}

@stop