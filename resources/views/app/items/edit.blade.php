@extends('layouts.main')
@section('title', 'Edit Item')

@section('content')

<div class="row">
	<div class="large-12 columns">
		<h1>Edit Item</h1>
		<hr>
	</div>
</div>

@if (session('success'))
<div class="row">
	<div class="callout success">
		{{ session('success') }}
	</div>
</div>
@endif

{!! Form::model($item, ['route' => ['items.update', $item->id], 'method' => 'PUT']) !!}
@include('partials.itemForm')
{!! Form::close() !!}
@stop