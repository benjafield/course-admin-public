@extends('layouts.main')
@section('title', 'Items')

@section('content')

	<div class="row">
		<div class="large-12">
			<h1>Items</h1>
			<hr>
			@can('manage_items')
			<div class="button-group">
				<a href="{{ route('items.create') }}" class="button">New Item</a>
			</div>
			@endcan
		</div>
	</div>

	@if (count($items) > 0)
		@foreach ($items as $item)
		<div class="row">
			<div class="large-12 columns callout secondary">
				<h5 class="small-8 columns"><a href="{{ route('items.edit', $item->id) }}">{{ $item->text }}</a></h5>
				{!! Form::open(['route' => ['items.destroy', $item->id], 'method' => 'DELETE']) !!}
				<button id="deleteItem{{ $item->id }}" class="button small alert float-right" type="submit">Delete</button>
				{!! Form::close() !!}
			</div>
		</div>
		@endforeach
	@else
		<div class="row">
			<div class="large-12 columns">
				<p>There are no items.</p>
			</div>
		</div>
	@endif

@stop