@extends('layouts.main')
@section('title', 'Edit: ' . $module->title)

@section('content')

<div class="row">
	<div class="large-12 columns">
		<h1>Edit: {{ $module->title }}</h1>
		<hr>
	</div>
</div>

@if (session('success'))
<div class="row">
	<div class="callout success">
		{{ session('success') }}
	</div>
</div>
@endif

{!! Form::model($module, ['route' => ['modules.update', $module], 'method' => 'PUT']) !!}

@include('partials.moduleForm')

{!! Form::hidden('redirectPath', route('modules.edit', $module->id)) !!}
{!! Form::close() !!}

@stop