@extends('layouts.main')
@section('title', $module->title)

@section('content')

	<div class="row">
		<div class="large-12 columns">
			<h1>{{ $module->title }} <small>Leader: {{ $module->leader->name }}</small></h1>
			<hr>
		</div>
	</div>

	@if(session('success'))
	<div class="row">
		<div class="callout success">
			{{ session('success') }}
		</div>
	</div>
	@endif

	<div class="row">
		<div class="large-6 columns">
			<h2>Items</h2>
			@if($module->items()->count() > 0)
			<ul class="no-bullet">
				@foreach($module->items as $item)
					
				<li class="clearfix">
					<h5 class="small-8 columns">{{ $item->text }}</h5>
					@if (Auth::user()->hasRole('module_leader'))
					{!! Form::model($item, ['route' => ['itemmodule.update', $item->id], 'method' => 'PUT', 'class' => 'small-2 columns text-right']) !!}
					{!! Form::hidden('completed', !$item->pivot->completed) !!}
					{!! Form::hidden('module_id', $module->id) !!}
					<button class="button small {{ (($item->pivot->completed) ? 'success' : 'warning') }}" type="submit">{{ (($item->pivot->completed) ? 'Complete' : 'Incomplete') }}</button>
					{!! Form::close() !!}
					@endif
					{!! Form::model($item, ['route' => ['itemmodule.destroy', $item->id], 'method' => 'DELETE', 'class' => 'small-2 columns text-right']) !!}
					{!! Form::hidden('module_id', $module->id) !!}
					<button class="button small alert" type="submit" id="deleteItem{{ $item->id }}">Delete</button>
					{!! Form::close() !!}
				</li>

				@endforeach
			</ul>
			@else
			<p>There are no items.</p>
			@endif
		</div>
		<div class="large-6 columns">
			<h2>Statistics</h2>
			<ul class="no-bullet">
				<li>
					Progress:
					<div class="progress" role="progressbar">
						@if ($module->items->count() > 0)
						<div class="progress-meter" style="width:{{ Maths::rPercentage($completedItems->count(), $module->items->count()) }}%;">
							<p class="progress-meter-text">{{ Maths::rPercentage($completedItems->count(), $module->items->count()) }}%</p>
						</div>
						@endif
					</div>
				</li>
				<li class="small-6 columns">Number of Items: <strong>{{ $module->items->count() }}</strong></li>
				<li class="small-6 columns">Completed Items: <strong>{{ $completedItems->count() }}</strong></li>
			</ul>

			<hr>
			<h2>Options</h2>
			
			@if(Auth::user()->hasRole('admin'))
			{!! Form::open(['route' => ['modules.destroy', $module->id], 'method' => 'DELETE']) !!}
			@endif
			<div class="button-group">
				<a class="button success" href="{{ url('modules/' . $module->id . '/item') }}">Add Item</a>
				@if(Auth::user()->hasRole('admin'))
				<a class="button" href="{{ route('modules.edit', $module->id) }}">Edit Module</a>
				<button class="button alert" type="submit">Delete Module</button>
				@endif
			</div>
			@if(Auth::user()->hasRole('admin'))
			{!! Form::close() !!}
			@endif
		</div>
	</div>

@stop