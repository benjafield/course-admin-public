@extends('layouts.main')
@section('title', 'Create Module')

@section('content')

	<div class="row">
		<div class="large-12 columns">
			<h1>Create Module</h1>
			<hr>
		</div>
	</div>

	{!! Form::open(['url' => 'modules']) !!}

	@include('partials.moduleForm')

	{!! Form::close() !!}

@stop