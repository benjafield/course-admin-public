@extends('layouts.main')
@section('title', 'Add Item to Module')

@section('content')
	
<div class="row">
	<div class="large-12 columns">
		<h1>{{ $module->title }} <small>Add an Item</small></h1>
		<hr>
	</div>
</div>

<div class="row">
	<div class="large-12 columns">
		<h3>Create New Item</h2>
		{!! Form::open(['url' => 'items', 'id' => 'newModuleItemForm']) !!}
		@include('partials.itemForm', ['module_id' => $module->id, 'default' => FALSE])
		{!! Form::hidden('redirectPath', url('modules/' . $module->id)) !!}
		{!! Form::close() !!}
	</div>

	@if(count($avItems) > 0)
	<h4 class="text-center">Or</h4>

	<div class="large-12 columns">
		<h3>Choose from the list</h3>

		{!! Form::open(['url' => 'itemmodule', 'id' => 'addItemToModuleForm']) !!}
		<div class="row">
			<div class="large-12 columns">
				<select name="item_id" id="item_id">
					<option value="">Select an Item...</option>
					@foreach($avItems as $item)
					<option value="{{ $item->id }}">{{ $item->text }}</option>
					@endforeach
				</select>
			</div>
		</div>

		<div class="row">
			<div class="large-12 columns">
				{!! Form::hidden('module_id', $module->id) !!}
				<button class="button expanded" type="submit">Add Item</button>
			</div>
		</div>
		{!! Form::close() !!}

	</div>
	@endif
</div>

@stop