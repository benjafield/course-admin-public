@extends('layouts.main')
@section('title', 'Modules')

@section('content')

	<div class="row">
		<div class="large-12">
			<h1>Modules</h1>
			<hr>
			@can('manage_modules')
			<div class="button-group">
				<a href="{{ route('modules.create') }}" class="button">New Module</a>
			</div>
			@endcan
		</div>
	</div>

	@if (count($modules) > 0)
		@foreach ($modules as $module)
		<div class="row">
			<div class="large-12 columns callout secondary">
				<h5><a href="{{ url('modules/' . $module->id) }}">{{ $module->title }}</a></h5>
				<p>Leader: {{ $module->leader->name }}</p>

				<?php
					$completedItems = $modulesCompleted->where('module_id', $module->id)->count();
					$totalItems = $module->items->count();
				?>

				<div class="progress" role="progressbar">
					@if($module->items->count() > 0)
					<?php
						$percentage = Maths::rPercentage($completedItems, $totalItems);
					?>
					<div class="progress-meter" style="width:{{ $percentage }}%">
						@if($percentage > 0)
						<span class="progress-meter-text">{{ $percentage }}%</span>
						@endif
					</div>
					@endif
				</div>
			</div>
		</div>
		@endforeach
	@else
		<div class="row">
			<div class="large-12 columns">
				<p>There are no courses.</p>
			</div>
		</div>
	@endif

@stop