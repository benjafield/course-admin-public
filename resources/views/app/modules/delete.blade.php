@extends('layouts.main')
@section('title', 'Delete Module')

@section('content')

<div class="row">
	<div class="large-12 columns">
		<h1>Delete Module: {{ $module->title }}</h1>
		<hr>
	</div>
</div>

{!! Form::open(['route' => ['modules.destroy', $module->id], 'method' => 'DELETE']) !!}

@include('partials.confirmDelete', ['label' => 'the module: ' . $module->title, 'abort' => 'modules/' . $module->id])

{!! Form::close() !!}

@stop