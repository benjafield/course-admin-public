@extends('layouts.main')
@section('title', $course->title)

@section('content')
	
	<div class="row">
		<div class="large-12 columns">
			<h1>{{ $course->title }} <small>Leader: {{ $course->leader->name }}</small></h1>
			<hr>
		</div>
	</div>

	<div class="row">
		<div class="large-6 columns">
			<h2>Modules</h2>
			@foreach ($course->modules as $module)
			<div class="callout secondary clearfix">
				<div class="small-8 columns">
					<h5><a href="{{ url('modules/' . $module->id) }}">{{ $module->title }}</a></h5>
					<p>Module Leader: {{ $module->leader->name }}</p>
				</div>
				@can('manage_modules')
				<div class="small-4 columns text-right">
					{!! Form::open(['route' => ['coursemodule.delete', $course->id], 'method' => 'DELETE']) !!}
					{!! Form::hidden('module_id', $module->id) !!}
					<button class="button alert small" type="submit">Remove</button>
					{!! Form::close() !!}
				</div>
				@endcan
			</div>
			@endforeach
		</div>

		<div class="large-6 columns">
			@if (Auth::user()->hasRole('course_leader') || Auth::user()->hasRole('admin'))
			<h2>Statistics</h2>
			<ul class="no-bullet">
				<li>
	
					<?php

						$totalModules = $course->modules->count();
						$compModules = ((isset($completedModules[$course->id])) ? count($completedModules[$course->id]) : 0);

					?>

					Progress:
					<div class="progress" role="progressbar">
						@if ($course->modules->count() > 0)
						<?php $percentage = Maths::rPercentage($compModules, $totalModules); ?>
						<div class="progress-meter" style="width:{{ $percentage }}%;">
							<p class="progress-meter-text">{{ $percentage }}%</p>
						</div>
						@endif
					</div>
				</li>
				<li class="small-6 columns">Number of Modules: <strong>{{ $totalModules }}</strong></li>
				<li class="small-6 columns">Completed Modules: <strong>{{ $compModules }}</strong></li>
			</ul>
			@endif
			
			@can('manage_courses')
			
			<h2>Options</h2>

			{!! Form::open(['route' => ['courses.destroy', $course->id], 'method' => 'DELETE']) !!}
			<div class="button-group">
				<a href="{{ url('courses/' . $course->id . '/module') }}" class="button success">Add Module</a>
				<a href="{{ route('courses.edit', $course->id) }}" class="button">Edit Course</a>
				<button type="submit" class="button alert">Delete Course</button>
			</div>
			{!! Form::close() !!}
			@endcan
		</div>
	</div>

@stop