@extends('layouts.main')
@section('title', 'Courses')

@section('content')

	<div class="row">
		<div class="large-12">
			<h1>Courses</h1>
			<hr>
			@can('manage_courses')
			<div class="button-group">
				<a href="{{ route('courses.create') }}" class="button">New Course</a>
			</div>
			@endcan
		</div>
	</div>

	@if(Auth::user()->hasRole('admin'))
		@include('partials.adminCourses')
	@elseif(Auth::user()->hasRole('course_leader'))
		@include('partials.courseLeaderCourses')
	@endif

@stop