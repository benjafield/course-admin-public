@extends('layouts.main')
@section('title', 'Delete Course')

@section('content')

<div class="row">
	<div class="large-12 columns">
		<h1>Delete Course: {{ $course->title }}</h1>
		<hr>
	</div>
</div>

{!! Form::open(['route' => ['courses.destroy', $course->id], 'method' => 'DELETE']) !!}

@include('partials.confirmDelete', ['label' => 'the course: ' . $course->title, 'abort' => 'courses/' . $course->id])

{!! Form::close() !!}

@stop