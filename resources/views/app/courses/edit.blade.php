@extends('layouts.main')
@section('title', 'Edit: ' . $course->title)

@section('content')

<div class="row">
	<div class="large-12 columns">
		<h1>Edit: {{ $course->title }}</h1>
		<hr>
	</div>
</div>

@if (session('success'))
<div class="row">
	<div class="callout success">
		{{ session('success') }}
	</div>
</div>
@endif

{!! Form::model($course, ['route' => ['courses.update', $course->id], 'method' => 'PUT']) !!}
@include('partials.courseForm')
{!! Form::close() !!}

@stop