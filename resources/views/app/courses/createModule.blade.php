@extends('layouts.main')
@section('title', 'Add Module to Course')

@section('content')

<div class="row">
	<div class="large-12 columns">
		<h1>{{ $course->title }} <small>Add Module</small></h1>
		<hr>
	</div>
</div>

{!! Form::open(['url' => 'modules']) !!}

@include('partials.moduleForm')
{!! Form::hidden('course_id', $course->id) !!}
{!! Form::hidden('redirectPath', url('courses/' . $course->id)) !!}

{!! Form::close() !!}

@if(count($avModules) > 0)
<div class="row">
	<div class="large-12 columns">
		<h4 class="text-center">Or</h4>
	</div>
</div>

<div class="row">
	<div class="large-12 columns">
		<h3>Choose a module from the list</h3>

		{!! Form::open(['url' => 'coursemodule']) !!}
		<div class="row">
			<div class="large-12 columns">
				<select name="module_id" id="module_id">
					<option value="">Select a Module...</option>
					@foreach($avModules as $module)
					<option value="{{ $module->id }}">{{ $module->title }}</option>
					@endforeach
				</select>
			</div>
		</div>

		<div class="row">
			<div class="large-12 columns">
				{!! Form::hidden('course_id', $course->id) !!}
				<button class="button expanded" type="submit">Add Module</button>
			</div>
		</div>
		{!! Form::close() !!}

	</div>
</div>
@endif

@stop