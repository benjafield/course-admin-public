@extends('layouts.main')
@section('title', 'Create Courses')

@section('content')

	<div class="row">
		<h1>Create a course</h1>
		<hr>
	</div>
	

	{!! Form::open(['url' => 'courses', 'id' => 'createCourseForm']) !!}
	@include('partials.courseForm')
	{!! Form::close() !!}

@stop