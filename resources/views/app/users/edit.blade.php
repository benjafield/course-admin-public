@extends('layouts.main')
@section('title', 'Edit: ' . $user->name)

@section('content')

<div class="row">
	<div class="large-12 columns">
		<h1>Edit: {{ $user->name }}</h1>
		<hr>
	</div>
</div>

{!! Form::model($user, ['route' => ['users.update', $user->id], 'method' => 'PUT']) !!}
@include('partials.userForm')
{!! Form::close() !!}

@stop