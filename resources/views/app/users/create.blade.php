@extends('layouts.main')
@section('title', 'Create User')

@section('content')

<div class="row">
	<div class="large-12 columns">
		<h1>Create User</h1>
		<hr>
	</div>
</div>

{!! Form::open(['route' => ['users.store']]) !!}
@include('partials.userForm')
{!! Form::close() !!}

@stop