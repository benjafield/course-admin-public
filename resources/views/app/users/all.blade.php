@extends('layouts.main')
@section('title', 'Users')

@section('content')

<div class="row">
	<div class="large-12 columns">
		<h1>Users</h1>
		<hr>
		<div class="button-group">
			<a href="{{ route('users.create') }}" class="button">New User</a>
		</div>
	</div>
</div>

<div class="row">
	<div class="large-12 columns">
		
		<table width="100%">
			<thead>
				<tr>
					<th width="100">User ID</th>
					<th>Name</th>
					<th>Email</th>
					<th width="150">Options</th>
				</tr>
			</thead>
			<tbody>
				@foreach($users as $user)
				<tr>
					<td>{{ $user->id }}</td>
					<td>{{ $user->name }}</td>
					<td>{{ $user->email }}</td>
					<td>
						{!! Form::open(['route' => ['users.destroy', $user->id], 'method' => 'DELETE']) !!}
						<div class="button-group tiny">
							<a href="{{ route('users.edit', $user->id) }}" class="button tiny">Edit</a>
							<button class="button alert tiny">Delete</button>
						</div>
						{!! Form::close() !!}
					</td>
				</tr>
				@endforeach
			</tbody>
		</table>

	</div>
</div>

@stop