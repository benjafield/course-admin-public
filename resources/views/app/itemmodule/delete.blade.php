@extends('layouts.main')
@section('title', 'Confirm Delete')

@section('content')

<div class="row">
	<div class="large-12 columns">
		<h1>Remove Item from Module</h1>
		<hr>
	</div>
</div>

{!! Form::open(['route' => ['itemmodule.destroy', $im->item_id], 'method' => 'DELETE']) !!}
<div class="row">
	<div class="large-12 columns">
		<div class="callout alert">
			<h3>Are you sure you want to remove this item?</h3>
			<div class="button-group">
				{!! Form::hidden('deleteConfirmed', 1) !!}
				{!! Form::hidden('module_id', $im->module_id) !!}
				<a href="{{ url('modules/' . $im->module_id) }}" class="button warning">No, Go Back</a>
				<button class="button alert" type="submit">Yes, Delete</button>
			</div>
		</div>
	</div>
</div>
{!! Form::close() !!}

@stop