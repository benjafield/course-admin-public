<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="UTF-8">
	<meta name="viewport" content="width=device-width, initial-scale=1, user-scalable=no">
	<meta name="author" content="Charlie Benjafield">
	<title>@yield('title') / CourseAdmin</title>
	<link rel="stylesheet" href="http://cdnjs.cloudflare.com/ajax/libs/foundicons/3.0.0/foundation-icons.css">
	{!! Html::style('css/app.css') !!}
</head>
<body>
	
	@include('partials.header')

	<main>
		@if (session('middlewareResponse'))
		<div class="row">
			<div class="callout alert">
				<p>{{ session('middlewareResponse') }}</p>
			</div>
		</div>
		@endif

		@yield('content')
	</main>

	@include('partials.footer')

	{!! Html::script('js/app.js') !!}
	<script>
		$(document).foundation();
	</script>

</body>
</html>