<?php

namespace App\Helpers;

class MathsHelper {

	/**
	 * Calculates a percentage based on the total
	 * and count provided. Rounds the result.
	 */
	public static function rPercentage($count, $total)
	{
		return round(($count/$total) * 100);
	}

	/**
	 * Calculates an actual percentage without
	 * rounding the result - could produce
	 * a lot of decimal places.
	 */
	public static function aPercentage($count, $total)
	{
		return ($count/$total) * 100;
	}

}