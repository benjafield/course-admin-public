<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use DB;

class Module extends Model
{
    
	/**
	 * The table the model will use
	 * 
	 * @var string $table
	 */
	protected $table = 'modules';

	/**
	 * Fields that are mass assignable
	 *
	 * @var array $fillable
	 */
	protected $fillable = [
		'title',
		'code',
		'leader_id'
	];

	/**
	 * Form the Eloquent relationship to courses
	 */
	public function courses()
	{
		return $this->belongsToMany('App\Course');
	}

	/**
	 * Form the Eloquent relationship to items
	 */
	public function items()
	{
		return $this->belongsToMany('App\Item')->withPivot('completed');
	}

	/**
	 * Form the Eloquent relation to the leader (user)
	 */
	public function leader()
	{
		return $this->belongsTo('App\User', 'leader_id');
	}

	/**
	 * Available Items
	 *
	 * Get the available items from the database
	 * which do not already have an association
	 * with the current module.
	 */
	public function availableItems()
	{

		/**
		 * Run query, 
		 * for some reason it doesn't like the Eloquent way...
		 */
		$sql = "SELECT * FROM items WHERE items.id NOT IN (SELECT item_id FROM item_module WHERE module_id = ?)";
		$avItems = DB::select($sql, [$this->id]);

		return $avItems;

	}

}
