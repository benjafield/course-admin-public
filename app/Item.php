<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Item extends Model
{
    
	/**
	 * The table the model will use
	 *
	 * @var string $table
	 */
	protected $table = 'items';

	/**
	 * Fields that are mass assignable
	 *
	 * @var array $fillable
	 */
	protected $fillable = [
		'text', 'default'
	];

	/**
	 * Form Eloquent relationship with modules
	 */
	public function modules()
	{
		return $this->belongsToMany('App\Module');
	}

}
