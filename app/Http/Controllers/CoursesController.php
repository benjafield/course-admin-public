<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use Auth;
use App\Course;
use App\User;
use App\Role;
use App\CourseModule;
use App\Http\Requests;
use App\Http\Controllers\Controller;

class CoursesController extends Controller
{

	/**
	 * Construct sets up the middlewares
	 */
	public function __construct()
	{
		$this->middleware('admin', ['only' => [
			'create', 'edit', 'destroy'
		]]);
	}

    /**
     * List of all courses
     */
	public function index()
	{

		$completedModules = Course::completedModules();

		$courses = Course::with('leader')->get();

		return view('app.courses.all', compact('courses', 'completedModules'));

	}

	/**
	 * Show the create course page
	 */
	public function create()
	{

		// Replace with middleware.
		if (!Auth::user()->can('manage_courses')) 
			return abort(403, 'You do not have permission to perform this action.');

		$users = Role::users('course_leader');

		return view('app.courses.create', compact('users'));

	}

	/**
	 * Store form data in storage
	 *
	 * @param Request $request
	 */
	public function store(Request $request)
	{

		/**
		 * Validate the request.
		 */
		$this->validate($request, [
			'title'     => 'required',
			'code'      => 'required|min:4|unique:courses',
			'leader_id' => 'required'
		]);

		/**
		 * Save the course
		 */
		$course = new Course($request->input());
		$course->save();

		return redirect('courses');

	}

	/**
	 * Show a module
	 *
	 * @param int $id
	 */
	public function show($id)
	{

		$course = Course::findOrFail($id);
		$completedModules = Course::completedModules($id);

		return view('app.courses.show', compact('course', 'completedModules'));

	}

	/**
	 * Destroy Course
	 */
	public function destroy(Request $request, $id)
	{

		$course = Course::findOrFail($id);
	
		if ($request->has('deleteConfirmed'))
		{
			$course->delete();
			/**
			 * Delete Course Module Associations
			 */
			CourseModule::where('course_id', $id)->delete();

			return redirect('courses')->with('success', 'The course was successfully deleted.');
		}

		return view('app.courses.delete', compact('course'));

	}

	/**
	 * Show the page to add a module
	 * to the course. Takes the
	 * course ID.
	 * 
	 * @param int $id
	 */
	public function createModule($id)
	{

		$course = Course::findOrFail($id);
		$avModules = $course->availableModules();
		$users = Role::users('module_leader');

		return view('app.courses.createModule', compact('course', 'avModules', 'users'));

	}

	/**
	 * Edit Course
	 *
	 * @param int $id
	 */
	public function edit($id)
	{

		$course = Course::findOrFail($id);
		$users = Role::users('course_leader');

		return view('app.courses.edit', compact('course', 'users'));

	}

	/**
	 * Update Function
	 *
	 * @param Request $request
	 * @param int $id
	 */
	public function update(Request $request, $id)
	{

		/**
		 * Validate the request
		 */
		$this->validate($request, [
			'title'     => 'required',
			'code'      => 'required|min:4',
			'leader_id' => 'required'
		]);

		/**
		 * Find the course or show 404.
		 */
		$course = Course::findOrFail($id);

		/**
		 * Update the course
		 */
		$course->update($request->input());

		return back()->with('success', 'The course was successfully updated.');

	}

}
