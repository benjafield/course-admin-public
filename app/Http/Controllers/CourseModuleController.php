<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\CourseModule;
use App\Http\Requests;
use App\Http\Controllers\Controller;

class CourseModuleController extends Controller
{
    
	/**
	 * Destroy a CourseModule association
	 *
	 * @param Request $request
	 * @param int $id
	 */
	public function destroy(Request $request, $id)
	{

		/**
		 * Get the module ID from request data.
		 */
		$module_id = $request->input('module_id');

		/**
		 * Find the association and delete it.
		 */
		CourseModule::where('course_id', $id)
		            ->where('module_id', $module_id)
		            ->delete();

		/**
		 * Return to the previous page.
		 */
		return back()->with('success', 'The module was removed from the course.');

	}

	/**
	 * Store CourseModule association in the
	 * database.
	 *
	 * @param Request $request
	 */
	public function store(Request $request)
	{

		$data = [
			'course_id' => $request->input('course_id'),
			'module_id' => $request->input('module_id'),
			'default'   => 0
		];

		$cm = new CourseModule($data);
		$cm->save();

		return redirect()->route('courses.show', $request->input('course_id'));

	}

}
