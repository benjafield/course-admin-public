<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use Carbon;
use DB;
use App\Module;
use App\ItemModule;
use App\Http\Requests;
use App\Http\Controllers\Controller;

class ItemModuleController extends Controller
{
    
    /**
     * Update whether an itemmodule has been
     * completed or not.
     */
	public function update(Request $request, $id)
	{

		/**
		 * Get the module and completed inputs
		 * from the form.
		 */
		$module_id = $request->get('module_id');
		$completed = $request->get('completed');

		/**
		 * Set the current timestamp.
		 */
		$now = Carbon\Carbon::now();

		/**
		 * Update the record in the database.
		 */
		DB::table('item_module')->where('module_id', $module_id)->where('item_id', $id)->update(['completed' => $completed, 'updated_at' => $now]);
		
		/**
		 * Redirect back to the previous page with
		 * a success message flashdata.
		 */
		return back()->with('success', 'Item was successfully updated.');

	}

	/**
	 * Destroy an itemmodule association.
	 *
	 * @param int $id
	 */
	public function destroy(Request $request, $id)
	{
		
		/**
		 * Get the module ID
		 */
		$module_id = $request->input('module_id');

		/**
		 * Confirm the delete
		 */
		if ($request->has('deleteConfirmed'))
		{
			ItemModule::where('item_id', $id)->where('module_id', $module_id)->delete();
			return redirect()->route('modules.show', $module_id)->with('success', 'The item has been successfully removed from this module.');
		}

		/**
		 * Get the association.
		 */
		$im = ItemModule::where('item_id', $id)
						->where('module_id', $module_id)
						->get()
						->first();

		/**
		 * Show the confirm page
		 */
		return view('app.itemmodule.delete', compact('im'));

	}

	/**
	 * Store an ItemModule Association to storage.
	 */
	public function store(Request $request)
	{

		$im = new ItemModule($request->input());
		$im->save();

		return redirect()->route('modules.show', $request->input('module_id'));

	}

}
