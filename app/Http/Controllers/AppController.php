<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use Auth;
use App\Http\Requests;
use App\Http\Controllers\Controller;

class AppController extends Controller
{
    
    public function index()
    {

        /**
         * If the user is logged in, show the dashboard.
         */
        if (Auth::check())
        {
            return $this->_authed();
        }

        /**
         * If the user is not logged in, show the login page.
         */
        return $this->_guest();

    }

    protected function _authed()
    {
        return view('app.dashboard');
    }

    protected function _guest()
    {
        return view('auth.login');
    }

}
