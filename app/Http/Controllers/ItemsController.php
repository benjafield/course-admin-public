<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Item;
use App\ItemModule;
use App\Http\Requests;
use App\Http\Controllers\Controller;

class ItemsController extends Controller
{
    
 	/**
 	 * Display all items
 	 */
	public function index()
	{

		/**
		 * Get all items
		 */
		$items = Item::get();

		return view('app.items.all', compact('items'));

	}

	/**
	 * Display the form to create an item.
	 */
	public function create() {
		
		return view('app.items.create');

	}

	/**
	 * Store Method
	 *
	 * @param Request 
	 */
	public function store(Request $request) {

		$this->validate($request, [
			'text' => 'required'
		]);

		/**
		 * Set the Item data
		 */
		$data = [
			'text' => $request->get('text'),
			'default' => $request->get('default')
		];
	
		$item = new Item($data);
		$item->save();

		if ($request->has('module_id'))
		{
			$im = new ItemModule;
			$im->item_id = $item->id;
			$im->module_id = $request->input('module_id');
			$im->save();
		}

		if ($request->has('redirectPath'))
		{
			return redirect($request->input('redirectPath'));
		}
	
		return redirect('items');
	
	}

	/**
	 * Edit Method
	 *
	 * @param int $id
	 */
	public function edit($id) {
		
		/**
		 * Get the Item or show 404 error page.
		 */
		$item = Item::findOrFail($id);

		/**
		 * Return the edit item view.
		 */
		return view('app.items.edit', compact('item'));
	
	}

	/**
	 * Update Method
	 *
	 * @param Request $request
	 * @param int $id
	 */
	public function update(Request $request, $id)
	{

		$item = Item::find($id);

		$item->update($request->input());

		return back()->with('success', 'The item has been successfully updated.');

	}

	/**
	 * Destroy an Item
	 *
	 * @param Request $request
	 * @param int $id
	 */
	public function destroy(Request $request, $id)
	{
		$item = Item::findOrFail($id);

		if ($request->has('deleteConfirmed'))
		{
			/**
			 * Delete the Item
			 */
			$item->delete();

			/**
			 * Delete all ItemModule Associations
			 */
			ItemModule::where('item_id', $id)->delete();

			/**
			 * Return to the items index
			 */
			return redirect('items');
		}

		return view('app.items.delete', compact('item'));

	}

}
