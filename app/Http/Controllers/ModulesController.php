<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Module;
use App\ItemModule;
use App\CourseModule;
use App\Role;
use App\Http\Requests;
use App\Http\Controllers\Controller;

class ModulesController extends Controller
{
    
    /**
     * Constructor sets up the middleware.
     */
    public function __construct()
	{
		$this->middleware('admin', ['only' => [
			'create', 'edit', 'destroy'
		]]);
	}

	/**
	 * List all modules
	 */
	public function index()
	{

		/**
		 * Get all modules
		 */
		if (auth()->user()->hasRole('module_leader'))
		{
			$modules = auth()->user()->modules;
		}
		else
		{
			$modules = Module::get();
		}

		/**
		 * Get the completed items.
		 */
		$modulesCompleted = ItemModule::where('completed', 1);

		return view('app.modules.all', compact('modules', 'modulesCompleted'));

	}

	/**
	 * Show an individual module
	 *
	 * @param int $id
	 */
	public function show($id)
	{

		/**
		 * Get the module info.
		 */
		$module = Module::findOrFail($id);

		/**
		 * Get the completed Items for this module.
		 */
		$completedItems = ItemModule::where('module_id', $id)->where('completed', 1);

		/**
		 * Return the modules show view.
		 */
		return view('app.modules.show', compact('module', 'completedItems'));

	}

	/**
	 * Show the page to create a module
	 */
	public function create()
	{

		$users = Role::users('module_leader');

		return view('app.modules.create', compact('users'));

	}

	/**
	 * Store the form data in storage.
	 */
	public function store(Request $request)
	{

		$this->validate($request, [
			'title'     => 'required',
			'code'      => 'required|min:4',
			'leader_id' => 'required'
		]);

		$module = new Module($request->input());
		$module->save();

		if ($request->has('course_id'))
		{
			$cm = new CourseModule;
			$cm->course_id = $request->input('course_id');
			$cm->module_id = $module->id;
			$cm->save();
		}

		if ($request->has('redirectPath'))
		{
			return redirect($request->input('redirectPath'));
		}

		return redirect('modules');

	}

	/**
	 * Destroy a module.
	 *
	 * @param int $id
	 */
	public function destroy(Request $request, $id)
	{

		$module = Module::findOrFail($id);
		$moduleTitle = $module->title;
		
		if ($request->has('deleteConfirmed'))
		{
			/**
			 * Delete the module
			 */
			$module->delete();

			/**
			 * Delete any ItemModule Associations
			 */
			ItemModule::where('module_id', $id)->delete();

			/**
			 * Redirect back to the module page
			 * with success message.
			 */
			return redirect('modules')->with('success', $moduleTitle . ' was successfully deleted.');
		}

		return view('app.modules.delete', compact('module'));

	}

	/**
	 * Create an itemmodule association
	 */
	public function createItem($id)
	{

		$module = Module::find($id);
		$avItems = $module->availableItems();

		return view('app.modules.createItem', compact('module', 'avItems'));

	}

	/**
	 * Edit Module
	 *
	 * @param int $id
	 */
	public function edit($id)
	{

		$users = Role::users('module_leader');
		$module = Module::findOrFail($id);

		return view('app.modules.edit', compact('module', 'users'));

	}

	/**
	 * Update Function
	 *
	 * @param Request $request
	 * @param int $id
	 */
	public function update(Request $request, $id)
	{

		/**
		 * Validate the request
		 */
		$this->validate($request, [
			'title'     => 'required',
			'code'      => 'required|min:4',
			'leader_id' => 'required'
		]);

		/**
		 * Find the module or show 404.
		 */
		$module = Module::findOrFail($id);

		/**
		 * Update the module
		 */
		$module->update($request->input());

		return back()->with('success', 'The module was successfully updated.');

	}

}
