<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\User;
use App\Role;
use App\Http\Controllers\Controller;

class UsersController extends Controller
{

    /**
     * Constructor sets up the middleware
     */
    public function __construct()
    {
        $this->middleware('admin');
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $users = User::get();

        return view('app.users.all', compact('users'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        
        $roles = Role::get();

        return view('app.users.create', compact('roles'));

    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $this->validate($request, [
            'name'     => 'required',
            'email'    => 'email|required|unique:users',
            'password' => 'required'
        ]);

        /**
         * Set the data to be added to the Users table.
         */
        $data = [
            'name'     => $request->input('name'),
            'email'    => $request->input('email'),
            'password' => bcrypt($request->input('password'))
        ];

        $user = new User($data);
        $user->save();

        /**
         * Check if a role has been supplied.
         */
        if ($request->has('user_role'))
        {
            /**
             * Give the newly created user the role.
             */
            $user->giveRole($request->input('user_role'));
        }

        return redirect()->route('users.index')->with('success', 'The user was successfully added.');

    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        
        $roles = Role::get();
        $user = User::findOrFail($id);

        return view('app.users.edit', compact('roles', 'user'));

    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        
        $user = User::findOrFail($id);

        $this->validate($request, [
            'name'     => 'required',
            'email'    => 'email|required|unique:users,email,' . $id
        ]);

        /**
         * Data to be update
         */
        $data = [
            'name'  => $request->input('name'),
            'email' => $request->input('email'),
        ];

        if (!empty($request->input('password')))
        {
            $data['password'] = bcrypt($request->input('password'));
        }

        $user->update($data);

        /**
         * Check if a role has been supplied.
         */
        if ($request->has('user_role'))
        {
            /**
             * Give the newly created user the role.
             */
            $user->giveRole($request->input('user_role'));
        }

        return redirect()->route('users.edit', $id)->with('success', 'The user was successfully updated.');

    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $user = User::findOrFail($id);
        $user->delete();
        return redirect('users');
    }
}
