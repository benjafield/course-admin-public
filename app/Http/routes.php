<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It's a breeze. Simply tell Laravel the URIs it should respond to
| and give it the controller to call when that URI is requested.
|
*/

/**
 * Public routes
 */
get('/', 'AppController@index');
get('login', 'Auth\AuthController@getLogin');
get('logout', 'Auth\AuthController@getLogout');
post('/login', 'Auth\AuthController@postLogin');

Route::group(['middleware' => 'auth'], function() {
	Route::resource('courses', 'CoursesController');
	Route::resource('modules', 'ModulesController');
	Route::resource('items', 'ItemsController');
	Route::resource('itemmodule', 'ItemModuleController');
	Route::delete('coursemodule/{id}', 'CourseModuleController@destroy')->name('coursemodule.delete');
	Route::resource('users', 'UsersController');
	post('coursemodule', 'CourseModuleController@store');
	get('modules/{id}/item', 'ModulesController@createItem');
	get('courses/{id}/module', 'CoursesController@createModule');
});
