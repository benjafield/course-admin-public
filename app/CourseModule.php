<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class CourseModule extends Model
{

   	/**
	 * The table the model will use
	 * 
	 * @var string $table
	 */
	protected $table = 'course_module';

	/**
	 * Fields that are mass assignable
	 *
	 * @var array $fillable
	 */
	protected $fillable = [
		'course_id',
		'module_id'
	];

	/**
	 * Form the Eloquent relationship to courses
	 */
	public function courses()
	{
		return $this->belongsToMany('App\Course');
	}

	/**
	 * Form the Eloquent relationship to module
	 */
	public function modules()
	{
		return $this->belongsTo('App\Module');
	}

}
