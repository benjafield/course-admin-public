<?php

namespace App;

use DB;
use App\Helpers\MathsHelper;
use Illuminate\Database\Eloquent\Model;

class Course extends Model
{
    
	/**
	 * The table the model will use
	 * 
	 * @var string $table
	 */
	protected $table = 'courses';

	/**
	 * Fields that are mass assignable
	 *
	 * @var array $fillable
	 */
	protected $fillable = [
		'title',
		'code',
		'leader_id'
	];

	/**
	 * Form the Eloquent relationship to modules
	 */
	public function modules()
	{
		return $this->belongsToMany('App\Module');
	}

	/**
	 * Form the Eloquent relationship to the course leader.
	 */
	public function leader()
	{
		return $this->belongsTo('App\User', 'leader_id');
	}

	/**
	 * Completed Modules
	 *
	 * get the completed modules
	 */
	public static function completedModules($course_id = '')
	{

		$courses = [];

		/**
		 * Make query raw - simpler to write.
		 */
		$sql = "SELECT course_module.course_id, course_module.module_id, (SELECT COUNT(*) FROM item_module WHERE item_module.module_id = course_module.module_id) AS totalItems, (SELECT COUNT(*) FROM item_module WHERE item_module.module_id = course_module.module_id AND item_module.completed = 1) AS completedItems FROM course_module LEFT JOIN courses ON courses.id = course_module.course_id";

		if (!empty($course_id))
		{
			$sql .= " WHERE course_module.course_id = ?";
			$result = DB::select($sql, [$course_id]);
		}
		else
		{
			$result = DB::select($sql);
		}

		/**
		 * Loop through the result and build array
		 * of completed modules under relevant
		 * course IDs.
		 */
		foreach ($result as $row)
		{
			if ($row->totalItems == $row->completedItems)
			{
				$courses[$row->course_id][] = $row;
			}
		}

		/**
		 * Return the result
		 */
		return $courses;

	}

	/**
	 * Available Modules
	 *
	 * Get the available modules from the database
	 * which do not already have an association
	 * with the current course.
	 */
	public function availableModules()
	{

		/**
		 * Run query, 
		 * for some reason it doesn't like the Eloquent way...
		 */
		$sql = "SELECT * FROM modules WHERE modules.id NOT IN (SELECT module_id FROM course_module WHERE course_id = ?)";
		$avModules = DB::select($sql, [$this->id]);

		return $avModules;

	}

}
