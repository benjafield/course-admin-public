<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ItemModule extends Model
{
    
	/**
	 * The table the model will use
	 * 
	 * @var string $table
	 */
	protected $table = 'item_module';

	/**
	 * Fields that are mass assignable
	 *
	 * @var array $fillable
	 */
	protected $fillable = [
		'module_id',
		'item_id',
		'completed'
	];

	/**
	 * Form the Eloquent relationship to modules
	 */
	public function modules()
	{
		return $this->belongsToMany('App\Module');
	}

	/**
	 * Form the Eloquent relationship to items
	 */
	public function items()
	{
		return $this->belongsTo('App\Item');
	}

}
