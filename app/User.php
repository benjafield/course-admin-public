<?php

namespace App;

use App\RoleUser;
use Illuminate\Auth\Authenticatable;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Auth\Passwords\CanResetPassword;
use Illuminate\Foundation\Auth\Access\Authorizable;
use Illuminate\Contracts\Auth\Authenticatable as AuthenticatableContract;
use Illuminate\Contracts\Auth\Access\Authorizable as AuthorizableContract;
use Illuminate\Contracts\Auth\CanResetPassword as CanResetPasswordContract;

class User extends Model implements AuthenticatableContract,
                                    AuthorizableContract,
                                    CanResetPasswordContract
{
    use Authenticatable, Authorizable, CanResetPassword;

    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'users';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = ['name', 'email', 'password'];

    /**
     * The attributes excluded from the model's JSON form.
     *
     * @var array
     */
    protected $hidden = ['password', 'remember_token'];

    /**
     * The relationship between user and their roles.
     */
    public function roles()
    {
        return $this->belongsToMany(Role::class);
    }

    /**
     * Courses
     */
    public function courses()
    {
        return $this->hasMany(Course::class, 'leader_id');
    }

    /**
     * Modules
     */
    public function modules()
    {
        return $this->hasMany(Module::class, 'leader_id');
    }

    /**
     * Check whether the user has a specified role.
     */
    public function hasRole($role)
    {

        if (is_string($role)) {
            return $this->roles->contains('name', $role);
        }

        return !! $role->intersect($this->roles)->count();

    }

    /**
     * Give user a role
     */
    public function giveRole($id)
    {
        /**
         * Remove current role association
         */
        RoleUser::where('user_id', $this->id)
                ->delete();

        /**
         * Grant the new role to the user.
         */
        $ur = new RoleUser([
            'user_id' => $this->id,
            'role_id' => $id
        ]);
        $ur->save();
        return TRUE;
    }

}
