<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateItemmoduleTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('item_module', function(Blueprint $table) {
            $table->integer('item_id')->unsigned();
            $table->integer('module_id')->unsigned();
            $table->boolean('completed');
            $table->primary(['item_id', 'module_id']);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('item_module');
    }
}
