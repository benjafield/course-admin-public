<?php

use Illuminate\Database\Seeder;

class ItemsSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        /**
         * Clear already existing data
         */
        DB::table('items');

        /**
         * Make 2 Items
         */
        $item = new App\Item([
            'text'    => 'Item 1',
            'default' => 0
        ]);
        $item->save();

        $item = new App\Item([
            'text'    => 'Item 2',
            'default' => 0
        ]);
        $item->save();

        /**
         * Add 2 Items to module 1
         */
        $im = new App\ItemModule([
            'item_id'   => 1,
            'module_id' => 1,
            'completed' => 1
        ]);
        $im->save();

        $im = new App\ItemModule([
            'item_id'   => 2,
            'module_id' => 1,
            'completed' => 0
        ]);

    }
}
