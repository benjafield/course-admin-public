<?php

use Illuminate\Database\Seeder;

class PermissionsSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        
    	$permissions = [
    		'manage_courses' => 'Manage Courses',      # 1
    		'manage_items'   => 'Manage Items',        # 2
    		'manage_modules' => 'Manage Modules',      # 3
    		'manage_users'   => 'Manage Users',        # 4
    		'view_modules'   => 'View Modules',        # 5
    		'view_module'    => 'View Module',         # 6
    		'complete_items' => 'Mark Items Complete', # 7
    		'delete_item'    => 'Delete Item',         # 8
    		'item_module'    => 'Add Item To Module',  # 9
    		'view_courses'   => 'View Courses',        # 10
    		'view_course'    => 'View Course',         # 11
    		'delete_course'  => 'Delete Course'        # 12
    	];

    	foreach ($permissions as $name => $label)
    	{
    		DB::table('permissions')->insert([
    			'name'  => $name,
    			'label' => $label
    		]);
    	}

    }
}
