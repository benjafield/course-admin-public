<?php

use Illuminate\Database\Seeder;

class PermissionRoleSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        
    	/**
    	 * Clear the data
    	 */
    	DB::table('permission_role')->truncate();

    	$adminPermissions = [
    		1, 2, 3, 4, 8, 12
    	];

    	$mlPermissions = [
    		5, 6, 7, 8, 9
    	];

    	$clPermissions = [
    		2, 5, 6, 10, 11
    	];

    	/**
    	 * Admin Permissions
    	 */
    	foreach ($adminPermissions as $perm)
    	{
    		DB::table('permission_role')->insert([
    			'permission_id' => $perm,
    			'role_id'       => 1
    		]); 
    	}

    	/**
    	 * Module Leader Permissions
    	 */
    	foreach ($mlPermissions as $perm)
    	{
    		DB::table('permission_role')->insert([
    			'permission_id' => $perm,
    			'role_id'       => 2
    		]);
    	}

    	/**
    	 * Course Leader Permissions
    	 */
    	foreach ($clPermissions as $perm)
    	{
    		DB::table('permission_role')->insert([
    			'permission_id' => $perm,
    			'role_id'       => 3
    		]); 
    	}

    }
}
