<?php

use Illuminate\Database\Seeder;

class UserSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        
    	/**
    	 * Clear data
    	 */
    	DB::table('users')->truncate();

    	factory('App\User', 10)->create();

    	$admin = new App\User([
    		'name'     => 'Admin',
    		'email'    => 'admin@example.com',
    		'password' => bcrypt('password')
    	]);
    	$admin->save();

    	$moduleleader = new App\User([
    		'name'     => 'Module Leader',
    		'email'    => 'moduleleader@example.com',
    		'password' => bcrypt('password')
    	]);
    	$moduleleader->save();

    	$courseleader = new App\User([
    		'name'     => 'Course Leader',
    		'email'    => 'courseleader@example.com',
    		'password' => bcrypt('password')
    	]);
    	$courseleader->save();

    }
}
