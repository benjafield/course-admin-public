<?php

use Illuminate\Database\Seeder;

class ModuleSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        
    	/**
    	 * Clear data
    	 */
    	DB::table('modules')->truncate();

    	$users = [
    		1, 2, 3, 4, 5, 6, 7, 8, 9, 10
    	];

    	for ($i = 1; $i < 11; $i++)
    	{
    		$userId = array_rand($users);

    		DB::table('modules')->insert([
    			'title'  => 'Module ' . $i,
    			'code'   => 'CIS' . rand(1000, 9999),
    			'leader_id' => $users[$userId]
    		]);
    	}

    }
}
