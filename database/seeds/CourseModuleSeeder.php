<?php

use Illuminate\Database\Seeder;

class CourseModuleSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        
    	/**
    	 * Clear data
    	 */
    	DB::table('course_module')->truncate();

    	$courses = [
    		1, 2, 3, 4
    	];

    	foreach ($courses as $course)
    	{
    		$modules = [
    			1, 2, 3, 4, 5, 6, 7, 8, 9, 10
    		];

    		$moduleLimit = rand(1, 11);

    		for ($i = 1; $i < $moduleLimit; $i++)
    		{
    			$moduleNum = array_rand($modules);

    			DB::table('course_module')->insert([
    				'course_id' => $course,
    				'module_id' => $modules[$moduleNum]
    			]);

    			unset($modules[$moduleNum]);
    		}
    	}

    }
}
