<?php

use Illuminate\Database\Seeder;

class RolesSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        
    	/**
    	 * Clear data
    	 */
    	DB::table('roles')->truncate();

    	$admin = new App\Role([
    		'name'  => 'admin',
    		'label' => 'Admin'
    	]);
        $admin->save();

    	$moduleleader = new App\Role([
    		'name'  => 'module_leader',
    		'label' => 'Module Leader'
    	]);
        $moduleleader->save();

    	$courseleader = new App\Role([
    		'name'  => 'course_leader',
    		'label' => 'Course Leader'
    	]);
        $courseleader->save();

    }
}
