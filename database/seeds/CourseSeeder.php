<?php

use Illuminate\Database\Seeder;

class CourseSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        
    	/**
    	 * Clear data
    	 */
    	DB::table('courses')->truncate();

    	$users = [
    		1, 2, 3, 4, 5, 6, 7, 8, 9, 10
    	];

    	$courses = [
    		'Computing',
    		'Software & Systems',
    		'Web Development',
    		'App Development'
    	];

    	foreach ($courses as $course)
    	{
    		$userId = array_rand($users);

    		DB::table('courses')->insert([
    			'title'  => $course,
    			'code'   => 'BIS' . rand(1000, 9999),
    			'leader_id' => $users[$userId]
    		]);
    	}

    }
}
