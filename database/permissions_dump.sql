INSERT INTO `permissions` (`id`, `name`, `label`, `created_at`, `updated_at`) VALUES
(1, 'manage_courses', 'Manage Courses', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(2, 'manage_items', 'Manage Items', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(3, 'manage_modules', 'Manage Modules', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(4, 'manage_users', 'Manage Users', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(5, 'view_modules', 'View Modules', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(6, 'view_module', 'View Module', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(7, 'complete_items', 'Mark Items Complete', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(8, 'delete_item', 'Delete Item', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(9, 'item_module', 'Add Item To Module', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(10, 'view_courses', 'View Courses', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(11, 'view_course', 'View Course', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(12, 'delete_course', 'Delete Course', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(13, 'crud_resources', 'Manage Resources', '0000-00-00 00:00:00', '0000-00-00 00:00:00');

-- --------------------------------------------------------
--
-- Dumping data for table `roles`
--

INSERT INTO `roles` (`id`, `name`, `label`, `created_at`, `updated_at`) VALUES
(1, 'admin', 'Admin', '2015-12-06 22:54:49', '2015-12-06 22:54:49'),
(2, 'module_leader', 'Module Leader', '2015-12-06 22:54:49', '2015-12-06 22:54:49'),
(3, 'course_leader', 'Course Leader', '2015-12-06 22:54:49', '2015-12-06 22:54:49');
